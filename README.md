# Palindrome Finder #

### Description ###
This project finds and prints the palindromes obtained with the permutation of words.

### Usage ###

* Pre-requisites: Java 8 and maven v3.3.3 or greater

Use the constant ```INPUT_WORDS``` in the class ```PalindromeFinder``` to specify the input words. The case will be ignored.

To execute the main class from Maven, use the command:
```
mvn exec:java -Dexec.mainClass="palindrome.PalindromeFinder"
```

A set of unit tests are provided.