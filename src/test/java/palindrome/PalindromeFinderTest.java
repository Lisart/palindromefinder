package palindrome;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;

/**
 * Test class for PalindromeFinder.
 *
 */
public class PalindromeFinderTest {

    PalindromeFinder palindromeFinder = new PalindromeFinder();

    /**
     * Test method for
     * {@link palindrome.PalindromeFinder#findPalindromes(java.lang.String[])}.
     */
    @Test
    public void testFindPalindromes() {
        String[] words = new String[] { "Gimli", "Fili", "Ilif" };
        Set<String> palindromes = palindromeFinder.findPalindromes(words);

        assertEquals(2, palindromes.size());
        assertTrue(palindromes.contains("iliffili"));
        assertTrue(palindromes.contains("filiilif"));
    }

    /**
     * Test method for
     * {@link palindrome.PalindromeFinder#findPalindromes(java.lang.String[])}.
     */
    @Test
    public void testFindPalindromesDuplicatedWord() {
        String[] words = new String[] { "aba", "aba" };
        Set<String> palindromes = palindromeFinder.findPalindromes(words);

        assertEquals(2, palindromes.size());
        assertTrue(palindromes.contains("aba"));
        assertTrue(palindromes.contains("abaaba"));
    }

    /**
     * Test method for
     * {@link palindrome.PalindromeFinder#findPalindromes(java.lang.String[])}.
     */
    @Test
    public void testFindPalindromesSingleWord() {
        String[] words = new String[] { "aba" };
        Set<String> palindromes = palindromeFinder.findPalindromes(words);

        assertEquals(1, palindromes.size());
        assertTrue(palindromes.contains("aba"));
    }

    /**
     * Test method for
     * {@link palindrome.PalindromeFinder#getPalindrome(java.lang.String[], int)}
     * .
     */
    @Test
    public void testGetPalindrome() {
        assertEquals("iliffili", palindromeFinder.getPalindrome(new String[] { "ilif", "fili" }, 2));
        assertEquals("iliffili", palindromeFinder.getPalindrome(new String[] { "ilif", "fili", "RANDOM" }, 2));
        assertNull(palindromeFinder.getPalindrome(new String[] { "Ilif", "Fili" }, 2));
        assertNull(palindromeFinder.getPalindrome(new String[] { "gimli", "fili", "ilif" }, 3));
    }

}
