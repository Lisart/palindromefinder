package palindrome;

import java.util.HashSet;
import java.util.Set;

/**
 * Class that finds and prints the palindromes obtained with the permutation of
 * words. Use the constant <code>INPUT_WORDS</code> to specify the input words.
 * The case will be ignored.
 * 
 * To execute this class from Maven, use the command:
 * <code>mvn exec:java -Dexec.mainClass="palindrome.PalindromeFinder"</code>
 * 
 * @author Haiyan Li
 *
 */
public class PalindromeFinder {

    /**
     * Array of input words.
     */
    static final String[] INPUT_WORDS = { "Gimli", "Fili", "Ilif", "Ilmig", "Mark" };

    /**
     * Main method.
     * 
     * @param args
     *            the parameters are ignored.
     */
    public static void main(String[] args) {

        PalindromeFinder palindromeFinder = new PalindromeFinder();

        // Finds the unique palindromes from the input words.
        Set<String> palindromes = palindromeFinder.findPalindromes(INPUT_WORDS);

        // Prints the palindromes found.
        palindromes.forEach(System.out::println);

    }

    /**
     * Finds the unique palindromes obtained with the permutation of words.
     * 
     * @param words
     *            words to use to find the palindromes.
     * @return set of palindrome strings.
     */
    public Set<String> findPalindromes(final String[] words) {

        String[] formattedWords = formatInputs(words);
        Set<String> palindromes = new HashSet<>();
        boolean[] usedWords = new boolean[formattedWords.length];
        String[] wordPermutation = new String[formattedWords.length];

        // Recursively find all the word permutations and evaluate if their
        // concatenation forms a palindrome.
        doPermute(palindromes, formattedWords, usedWords, wordPermutation, 0);

        // Return the palindromes
        return palindromes;
    }

    /**
     * Converts the input words to lower case.
     * 
     * @param words
     *            input words to be formated.
     * @return input words in lower case.
     */
    private String[] formatInputs(String[] words) {
        String[] formattedInputs = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            formattedInputs[i] = words[i].toLowerCase();
        }
        return formattedInputs;

    }

    /**
     * Recursively find all the word permutations and evaluate if their
     * concatenation forms a palindrome.
     * 
     * @param palindromes
     *            set of palindromes found.
     * @param words
     *            words to permute, case sensitive.
     * @param usedWords
     *            array that tracks which words are already used in the
     *            permutation.
     * @param wordPermutation
     *            permutation of words.
     * @param level
     *            depth of the recursion. Starts at 0.
     */
    private void doPermute(final Set<String> palindromes, final String[] words, final boolean[] usedWords,
            final String[] wordPermutation, final int level) {

        if (level < words.length) {
            for (int i = 0; i < words.length; i++) {
                if (!usedWords[i]) {
                    wordPermutation[level] = words[i];

                    String palindrome = getPalindrome(wordPermutation, level + 1);
                    if (palindrome != null) {
                        palindromes.add(palindrome);
                    }

                    usedWords[i] = true;
                    doPermute(palindromes, words, usedWords, wordPermutation, level + 1);
                    usedWords[i] = false;
                }
            }
        }
    }

    /**
     * Concatenates the words permutation and verifies whether it is a
     * palindrome. If yes, it returns the palindrome string, otherwise it
     * returns null.
     * 
     * @param wordPermutation
     *            array with a permutation of words.
     * @param wordsInPermutation
     *            number of words in the permutation.
     * @return the palindrome string or null.
     */
    public String getPalindrome(String[] wordPermutation, int wordsInPermutation) {

        // Concatenates the words in the permutation
        StringBuilder concatenatedWords = new StringBuilder();
        for (int i = 0; i < wordsInPermutation; i++) {
            concatenatedWords.append(wordPermutation[i]);
        }

        // Verifies if the concatenation is a palindrome
        int index = 0;
        int length = concatenatedWords.length();
        for (index = 0; index < length / 2; index++) {
            if (concatenatedWords.charAt(index) != concatenatedWords.charAt(length - index - 1)) {
                return null;
            }
        }

        return concatenatedWords.toString();
    }
}
